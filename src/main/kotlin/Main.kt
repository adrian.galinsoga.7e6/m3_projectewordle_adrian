import java.util.*



fun main() {
    val wordsES = arrayOf(
        "vamos",
        "ahora",
        "estoy",
        "tengo",
        "estás",
        "puedo",
        "tiene",
        "bueno",
        "hacer",
        "todos",
        "puede",
        "sabes",
        "quién",
        "nunca",
        "dónde",
        "favor",
        "señor",
        "mejor",
        "están",
        "usted",
        "mucho",
        "hasta",
        "sobre",
        "decir",
        "cosas",
        "antes",
        "estar",
        "noche",
        "nadie",
        "padre",
        "gente",
        "donde",
        "mismo",
        "hecho",
        "ellos",
        "claro",
        "estas",
        "lugar",
        "mundo",
        "amigo",
        "desde",
        "fuera",
        "tener",
        "crees",
        "buena",
        "gusta",
        "nuevo",
        "había",
        "menos",
        "tenía",
        "madre",
        "quien",
        "luego",
        "todas",
        "mujer",
        "visto",
        "haces",
        "tarde",
        "parte",
        "haber",
        "saber",
        "veces",
        "tanto",
        "razón",
        "quizá",
        "estos",
        "salir",
        "hemos",
        "chica",
        "entre",
        "algún",
        "serio",
        "somos",
        "pensé",
        "debes",
        "forma",
        "chico",
        "dicho",
        "nueva",
        "sabía",
        "ayuda",
        "hacia",
        "miedo",
        "adiós",
        "poder",
        "niños",
        "sería",
        "viejo",
        "manos",
        "pasar",
        "viene",
        "horas",
        "listo",
        "único",
        "cerca",
        "otros",
        "sigue",
        "habla",
        "feliz",
        "llama",
        "venir",
        "morir",
        "atrás",
        "dices",
        "abajo",
        "dejar",
        "tomar",
        "justo",
        "juego",
        "matar",
        "cinco",
        "dicen",
        "clase",
        "pueda",
        "igual",
        "venga",
        "creer",
        "saben",
        "hagas",
        "comer",
        "punto",
        "misma",
        "vivir",
        "queda",
        "hijos",
        "mayor",
        "hacen",
        "medio",
        "basta",
        "meses",
        "fácil",
        "final",
        "lista",
        "trata",
        "armas",
        "podía",
        "perro",
        "fuego"
    )
    val wordsEN = arrayOf(
        "never",
        "where",
        "about",
        "night",
        "place",
        "world",
        "after",
        "woman",
        "among",
        "would",
        "hours",
        "ready",
        "happy",
        "could",
        "abuse",
        "angel",
        "apple",
        "arrow",
        "basic",
        "beard",
        "beast",
        "black",
        "blaze",
        "bloom",
        "blunt",
        "bonus",
        "boost",
        "bored",
        "bound",
        "brain",
        "brave",
        "brick",
        "brief",
        "broth",
        "brush",
        "build",
        "burst",
        "cable",
        "calmly",
        "camel",
        "candy",
        "caper",
        "carat",
        "cards",
        "carry",
        "catch",
        "cause",
        "chain",
        "chalk",
        "charm",
        "cheap",
        "cheer",
        "chess",
        "chips",
        "chose",
        "chuck",
        "chunk",
        "civil",
        "clasp",
        "climb",
        "clown",
        "coach",
        "coast",
        "color",
        "comet",
        "comic",
        "crack",
        "craft",
        "crash",
        "crawl",
        "crazy",
        "cream",
        "crisp",
        "cross",
        "crowd",
        "crown",
        "crumb",
        "crush",
        "curve",
        "cycle",
        "daddy",
        "dance",
        "dazed",
        "death",
        "debut",
        "decay",
        "decor",
        "delay",
        "depth",
        "derby",
        "diary",
        "diner",
        "dirty",
        "ditch",
        "diver",
        "dizzy",
        "drain",
        "drift",
        "drill",
        "drink",
        "drive",
        "drove",
        "dwarf",
        "eager",
        "eagle",
        "early",
        "earth",
        "easel",
        "eaten",
        "eight",
        "elbow",
        "elder",
        "elect",
        "elite",
        "empty",
        "enemy",
        "enjoy",
        "enter",
        "envoy",
        "equal",
        "error",
        "essay",
        "event",
        "every",
        "extra",
        "fable",
        "faith",
        "fancy",
        "fatal",
        "favor",
        "fence",
        "fetch",
        "fever",
        "field",
        "fifty",
        "fight",
        "filth",
        "final",
        "first",
        "fizzy",
        "flame",
        "flask",
        "fling",
        "float",
        "flock",
        "floor",
        "flour",
        "flush",
        "focal",
        "focus",
        "folks",
        "force",
        "forge",
        "forth",
        "found",
        "frame",
        "frank",
        "fraud",
        "freak",
        "fresh",
        "fried",
        "front",
        "frost",
        "frown",
        "fruit",
        "fully",
        "funny",
        "gains",
        "gates",
        "gauge",
        "ghost",
        "giant",
        "gifts",
        "giver",
        "glare",
        "glass",
        "glaze",
        "gleam"
    )

    val palabra: String = wordsES.random()
//el 48 pinta el fondo de la palabra y el 38 la palabra
    val green = "\u001b[48;2;0;204;0m"       //fondo verde
    val verd = "\u001b[38;2;0;204;0m"        //letras verdes al ganar
    val yell = "\u001b[48;2;240;234;39m"     //fondo amarillo
    val grey = "\u001b[48;2;160;160;160m"    //fonde gris
    val white = "\u001b[48;2;255;255;255m"   //fondo blanco
    val blanc = "\u001b[38;2;255;255;255m"   //letras blancas
    val red = "\u001b[38;2;255;0;0m"         //letras rojas
    val black = "\u001b[38;2;0;0;0m"          //letras negras
    val reset = "\u001b[m"                  //"corta" el color del fondo y el de las palabras, de forma que las siguientes palabras no aparezcan pintadas
    var j: Int                               //añade las filas con las letras y sus respectivos colores
    var play: String                         //jugada del usuario
    var repeat: String                       //variable que permitirá jugar al usuario otra vez
    var contadoRondas: Int = 0                //numero de rondas jugadas
    var contadorPalabrasAcertadas: Int = 0    //palabras acertadas
    var contadorPalabrasErroneas: Int = 0     //palabras no acertadas
    var intentos = 0                        //total de intentos en cada ronda


    val scanner = Scanner(System.`in`)
    println(
        white + black + "QUE IDIOMA QUIERES SELECCIONAR?\n" +
                "INGLÉS O ESPAÑOL"
    )
    val language = scanner.next().lowercase()

    while (language == "INGLES".lowercase()) {
        do {
            val palabra = wordsEN.random() //palabra al azar
            var tries = 6 //intentos
            println("\n") //salto de línea
            println(
                "                 ___         _________    ________   ____   ___      ______ \n" +
                        "            \\ \\/  _ \\/ /    |  ____   |  |   .   |  |    \\  |  |     |  ===|\n" +
                        "             \\   / \\   /    |  |___|  |  |    ---'    |  |  |  |___  |  ===   \n" +
                        "              \\_/   \\_/     |_________|  |__| \\__|  |____/  |______| |_____|\n" + reset
            )
            println("\n")

            println("To read the game instructions press (i)")


            val instruccions = scanner.next().lowercase()
            if (instruccions == "i") {
                println(
                    "- The target word is randomly selected by the program and the player must enter a 5-letter word each time. \n " +
                            "- After each try, the program evaluates the word and shows the player how many letters match the target word and how many are in the correct position. \n" +
                            "- The player must guess the target word in as few guesses as possible. If the player guesses the target word, a congratulatory message is displayed and the game ends.\n"
                )
            } else {
                println(white + black + "STARTING PROGRAM..." + reset)
            }
            println(white + black + "START GAME: ENTER THE 5 LETTER WORD")
            do {
                play = readLine()!!.toString() //pide palabra al usuario
                j = 0

                if (play.length == 5) { //comprueba que las palabras contengan 5 letras
                    for (i in play) {
                        if (palabra[j] == i) {
                            print(green + black + i + reset) //si la letra está en su sitio, aparecerá verde
                        } else if (palabra.contains(i)) {
                            print(yell + black + i + reset) //si la letra está mal colocada, aparecerá amarilla
                        } else {
                            print(grey + black + i + reset) //si la palabra no contiene esa letra, aparecerá gris
                        }
                        j++
                    }
                    println(" ")
                    tries-- //se van restando los intentos
                    println("Attempts:$tries \n") //mostra els intents realitzats
                } else println(red + "5 letter word") //advertencia de que no se ha introducido una palabra de 5 letras

            } while (tries > 0 && play != palabra) //se termina el juego si aciertas la palabra o si se acxaban el número de intentos

            if (tries >= 0 && play != palabra) {
                println(red + "Oh no, the word was $palabra" + reset) //mensaje al no tener más intentos y muestra la palabra que era
            } else if (play == palabra) {
                contadorPalabrasAcertadas++
                println("Partidas Ganadas: $contadorPalabrasAcertadas")
                println(verd + "Congratulations!!!!!!" + reset) //mensaje al ganar
            }

            println(blanc + "Do you want to play again? Press y to play again or n to stop playing." + reset) //opción para volver a jugar

            repeat = readLine()!!.toString()
        } while (repeat == "y") //si pulsa la letra "s" volverá a jugar; y si pulsa cualquier otra, finaliza el juego

        if (repeat != "y") println(blanc + "Thanks for playing") //mensaje al finalizar del juego
        break
    }





    while (language == "ESPAÑOL".lowercase()) {
        println("Para leer las instrucciones del juego pulse (i)")
        val instruccions = scanner.next().lowercase()
        if (instruccions == "i") {
            println(
                "- La palabra objetivo es seleccionada al azar por el programa y el jugador debe ingresar una palabra de 5 letras en cada intento. \n " +
                        "- Después de cada intento, el programa evalúa la palabra y muestra al jugador cuántas letras coinciden con la palabra objetivo y cuántas están en la posición correcta.\n" +
                        "- El jugador debe adivinar la palabra objetivo en el menor número posible de intentos. Si el jugador adivina la palabra objetivo, se muestra un mensaje de felicitación y el juego termina.\n"
            )
        } else {
            println(white + black + "INICIANDO PROGRAMA..." + reset)
        }
        do {
            val palabra = wordsES.random() //palabra al azar
            var tries = 6 //intentos
            println("\n") //salto de línea
            println(
                "                 ___         _________    ________   ____   ___      ______ \n" +
                        "            \\ \\/  _ \\/ /    |  ____   |  |   .   |  |    \\  |  |     |  ===|\n" +
                        "             \\   / \\   /    |  |___|  |  |    ---'    |  |  |  |___  |  ===   \n" +
                        "              \\_/   \\_/     |_________|  |__| \\__|  |____/  |______| |_____|\n" + reset
            )
            println("\n")

            println(white + black + "COMIENZA EL JUEGO: INTRODUCE LA PALABRA DE 5 LETRAS")
            do {
                play = readLine()!!.toString() //pide palabra al usuario
                j = 0

                if (play.length == 5) { //comprueba que las palabras contengan 5 letras
                    for (i in play) {
                        if (palabra[j] == i) {
                            print(green + black + i + reset) //si la letra está en su sitio, aparecerá verde
                        } else if (palabra.contains(i)) {
                            print(yell + black + i + reset) //si la letra está mal colocada, aparecerá amarilla
                        } else {
                            print(grey + black + i + reset) //si la palabra no contiene esa letra, aparecerá gris
                        }
                        j++
                    }
                    println(" ")
                    tries-- //se van restando los intentos
                    println("Intentos:$tries \n") //mostra els intents realitzats
                } else println(red + "Palabra de 5 letras") //advertencia de que no se ha introducido una palabra de 5 letras

            } while (tries > 0 && play != palabra) //se termina el juego si aciertas la palabra o si se acxaban el número de intentos

            if (tries >= 0 && play != palabra) {
                println(red + "Oh no, la palabra era $palabra" + reset) //mensaje al no tener más intentos y muestra la palabra que era
            } else if (play == palabra) {
                println(verd + "Enhorabuena!!!" + reset) //mensaje al ganar
            }
            println(blanc + "Quieres volver a jugar? Pulsa s Para volver a jugar o n para dejar de jugar." + reset) //opción para volver a jugar

            repeat = readLine()!!.toString()
        } while (repeat == "s") //si pulsa la letra "s" volverá a jugar; y si pulsa cualquier otra, finaliza el juego

        if (repeat != "s") println(blanc + "Gracias por jugar") //mensaje al finalizar del juego
        break
    }

}


